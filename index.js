const express = require("express");
const path = require("path");

const app = express();

app.use("/static", express.static(path.resolve(__dirname, "dist", "static")));
// app.use(express.static(__dirname + '/'));

app.get("/*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "dist", "index.html"));
});

const PORT = 8000
app.listen(process.env.PORT || PORT, () => console.log("Server running in http://localhost:8000"));