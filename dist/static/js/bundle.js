const navigateTo=e=>{history.pushState(null,null,e),router()},router=async()=>{const e=[{path:"/",view:async()=>{return{title:"ChatRoom",content:await getView("/static/pages/index.html")}}},{path:"/login",view:async()=>{return{title:"Login",content:await getView("/static/pages/login.html")}}},{path:"/home",view:async()=>{return{title:"Home",content:await getView("/static/pages/home.html")}}}],t=e.map(e=>({route:e,isMatch:location.pathname===e.path}));let a=t.find(e=>e.isMatch);var l=await(a=a||{route:e[0],isMatch:!0}).route.view();document.title=l.title,document.querySelector("#root").innerHTML=l.content,"/"==a.route.path?handleLinkScroll():"/home"==a.route.path&&(handleGetPost(),handleAddPost()),a.route.view()};function getView(e){return fetch(e).then(e=>e.text())}function handleLinkScroll(){const e=[...document.querySelectorAll(".link-scroll")];e.map(t=>{t.addEventListener("click",e=>{e.preventDefault();e=t.href.split("#")[1],e=document.querySelector("#"+e);window.scrollTo({top:e.offsetTop-100,behavior:"smooth"})})})}function handleReply(){const e=[...document.querySelectorAll(".btn-reply")];e.map(r=>{r.addEventListener("click",e=>{var t=r.previousElementSibling.value;if(""!=t&&"undefined"!=typeof Storage&&null!==localStorage.getItem("posts")){const l=JSON.parse(localStorage.getItem("posts"));t={id:Date.now(),nama:"Ari Yuhendra",username:"myndra1805",created_at:new Date,replies:[],message:t};const s=r.dataset.id,i=l.filter(e=>e.id==s);var a=l.indexOf(i[0]);if(1===i.length){i[0].replies.push(t),l[a]=i[0],localStorage.setItem("posts",JSON.stringify(l));const n=document.querySelector("#reply-"+s),o=JSON.parse(localStorage.getItem("posts"));t=o.filter(e=>e.id==s);n.innerHTML=getAllReplies(t[0].replies),r.previousElementSibling.value=""}}})})}function handleAddPost(){const e=document.querySelector("#btn-save"),l=document.querySelector("#message");e.addEventListener("click",e=>{var t=l.value;if(""!=t&&"undefined"!=typeof Storage){if(null===localStorage.getItem("posts"))localStorage.setItem("posts",JSON.stringify([{id:Date.now(),nama:"Ari Yuhendra",username:"myndra1805",created_at:new Date,replies:[],message:t}]));else{const a=JSON.parse(localStorage.getItem("posts"));a.push({id:Date.now(),nama:"Ari Yuhendra",username:"myndra1805",created_at:new Date,replies:[],message:t}),localStorage.setItem("posts",JSON.stringify(a))}l.value="",handleGetPost()}})}function handleGetPost(){let e=[];"undefined"!=typeof Storage&&null!==localStorage.getItem("posts")&&(e=JSON.parse(localStorage.getItem("posts")));const t=document.querySelector("#accordionPost");let a="";(e=e.reverse()).map(e=>{a+=template(e)}),t.innerHTML=a,handleReply()}window.addEventListener("popstate",router),document.addEventListener("DOMContentLoaded",()=>{document.body.addEventListener("click",e=>{e.target.matches("[data-link]")&&(e.preventDefault(),navigateTo(e.target.href))}),router()});const template=e=>{var t=getAllReplies(e.replies);return`
    <div class="my-2">
        <div class="card bg-danger my-3 text-white">
            <div class="card-header" id="heading-${e.id}">
                <div class="d-flex align-items-center">
                    <img src="https://ui-avatars.com/api/?name=rudi" class="rounded-circle" width="50px"
                        alt="">
                    <div class="ml-2" style="line-height: 15px;">
                        <p class="m-0 font-weight-bold">${e.nama}</p>
                        <small class="m-0">${e.username}</small>
                    </div>
                </div>
                <div class="mt-3">
                    <h6>${e.message}</h6>
                    <small class="">${e.created_at.split("T")[0]}</small>
                </div>
                <div class="text-right">
                    <button class="btn btn-outline-light btn-sm" style="cursor: pointer;"
                        data-toggle="collapse" data-target="#collapse-${e.id}" aria-expanded="true"
                        aria-controls="collapse-${e.id}">Reply</button>
                </div>
            </div>
            <div id="collapse-${e.id}" class="collapse bg-white text-dark" aria-labelledby="heading-${e.id}"
                data-parent="#accordionPost">
                <div class="card-body">
                    <ul class="list-group pl-5 list-group-flush" id="reply-${e.id}">
                        ${t}
                    </ul>
                    <div class="form-group text-right mt-4">
                        <textarea class="form-control" placeholder="Masukkan komentar"
                            rows="3"></textarea>
                        <button data-id="${e.id}" type="button" class="btn btn-danger btn-sm mt-2 btn-reply">
                            <i class="bi bi-send"></i>
                            Send
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `};function getAllReplies(e){let t="";return e.map(e=>{t+=`
        <li class="list-group-item" style="border-color: #dc3545;">
            <div class="d-flex align-items-center">
                <img src="https://ui-avatars.com/api/?name=rudi" class="rounded-circle"
                    width="50px" alt="">
                <div class="ml-2" style="line-height: 15px;">
                    <p class="m-0 font-weight-bold">${e.nama}</p>
                    <small class="m-0">${e.username}</small>
                </div>
            </div>
            <div class="mt-3">
                <h6>${e.message}</</h6>
                <small class="">${e.created_at.split("T")[0]}</small>
            </div>
        </li>
        `}),t}