const navigateTo = url => {
    history.pushState(null, null, url)
    router();
}

const router = async () => {
    const routes = [
        {
            path: '/', view: async () => {
                const view = await getView('/static/pages/index.html')
                return {
                    title: 'ChatRoom',
                    content: view
                }
            }
        },
        {
            path: '/login', view: async () => {
                const view = await getView('/static/pages/login.html')
                return {
                    title: 'Login',
                    content: view
                }
            }
        },
        {
            path: '/home', view: async () => {
                const view = await getView('/static/pages/home.html')
                return {
                    title: 'Home',
                    content: view
                }
            }
        },
    ]

    const potentialMatches = routes.map(route => {
        return {
            route: route,
            isMatch: location.pathname === route.path
        }
    })

    let match = potentialMatches.find(potentialMatch => potentialMatch.isMatch)

    if (!match) {
        match = {
            route: routes[0],
            isMatch: true
        }
    }
    const view = await match.route.view()
    document.title = view.title
    document.querySelector("#root").innerHTML = view.content
    if (match.route.path == '/') {
        handleLinkScroll()
    } else if (match.route.path == '/home') {
        handleGetPost()
        handleAddPost()
    }
    match.route.view()
}

window.addEventListener("popstate", router)

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", event => {
        if (event.target.matches("[data-link]")) {
            event.preventDefault()
            navigateTo(event.target.href)
        }
    })
    router()
})

function getView(url) {
    return fetch(url).then(response => response.text())
}


function handleLinkScroll() {
    const linksScroll = [...document.querySelectorAll(".link-scroll")]
    linksScroll.map(btn => {
        btn.addEventListener("click", event => {
            event.preventDefault()
            const tujuan = btn.href.split("#")[1]
            const elementTujuan = document.querySelector(`#${tujuan}`)
            window.scrollTo({
                top: elementTujuan.offsetTop - 100,
                behavior: 'smooth',
            })
        })
    })
}

function handleReply() {
    const btnReply = [...document.querySelectorAll(".btn-reply")]
    btnReply.map(btn => {
        btn.addEventListener("click", event => {
            const value = btn.previousElementSibling.value
            if (value != '') {
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.getItem('posts') !== null) {
                        const posts = JSON.parse(localStorage.getItem("posts"))
                        const item = {
                            id: Date.now(),
                            nama: 'Ari Yuhendra',
                            username: 'myndra1805',
                            created_at: new Date(),
                            replies: [],
                            message: value
                        }
                        const id = btn.dataset.id
                        const result = posts.filter(post => post.id == id)
                        const index = posts.indexOf(result[0])
                        if (result.length === 1) {
                            result[0].replies.push(item)
                            posts[index] = result[0]
                            localStorage.setItem('posts', JSON.stringify(posts))
                            const elementReply = document.querySelector("#reply-" + id)
                            const newPosts = JSON.parse(localStorage.getItem("posts"))
                            const newResult = newPosts.filter(post => post.id == id)
                            elementReply.innerHTML = getAllReplies(newResult[0].replies)
                            btn.previousElementSibling.value = ''
                        }
                    }
                }
            }
        })
    })
}

function handleAddPost() {
    const btn = document.querySelector("#btn-save")
    const message = document.querySelector("#message")
    btn.addEventListener("click", event => {
        const value = message.value
        if (value != '') {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.getItem('posts') === null) {
                    localStorage.setItem('posts', JSON.stringify([
                        {
                            id: Date.now(),
                            nama: 'Ari Yuhendra',
                            username: 'myndra1805',
                            created_at: new Date(),
                            replies: [],
                            message: value
                        }
                    ]))
                } else {
                    const items = JSON.parse(localStorage.getItem("posts"))
                    items.push({
                        id: Date.now(),
                        nama: 'Ari Yuhendra',
                        username: 'myndra1805',
                        created_at: new Date(),
                        replies: [],
                        message: value
                    })
                    localStorage.setItem("posts", JSON.stringify(items))
                }
                message.value = ""
                handleGetPost()
            }
        }
    })
}

function handleGetPost() {
    let posts = []
    if (typeof (Storage) !== "undefined") {
        if (localStorage.getItem('posts') !== null) {
            posts = JSON.parse(localStorage.getItem('posts'))
        }
    }
    const contentPost = document.querySelector("#accordionPost")
    let tmp = ''
    posts = posts.reverse()
    posts.map(post => {
        tmp += template(post)
    })
    contentPost.innerHTML = tmp
    handleReply()
}

const template = (post) => {
    let reply = getAllReplies(post.replies)
    return  /*html*/`
    <div class="my-2">
        <div class="card bg-danger my-3 text-white">
            <div class="card-header" id="heading-${post.id}">
                <div class="d-flex align-items-center">
                    <img src="https://ui-avatars.com/api/?name=rudi" class="rounded-circle" width="50px"
                        alt="">
                    <div class="ml-2" style="line-height: 15px;">
                        <p class="m-0 font-weight-bold">${post.nama}</p>
                        <small class="m-0">${post.username}</small>
                    </div>
                </div>
                <div class="mt-3">
                    <h6>${post.message}</h6>
                    <small class="">${post.created_at.split("T")[0]}</small>
                </div>
                <div class="text-right">
                    <button class="btn btn-outline-light btn-sm" style="cursor: pointer;"
                        data-toggle="collapse" data-target="#collapse-${post.id}" aria-expanded="true"
                        aria-controls="collapse-${post.id}">Reply</button>
                </div>
            </div>
            <div id="collapse-${post.id}" class="collapse bg-white text-dark" aria-labelledby="heading-${post.id}"
                data-parent="#accordionPost">
                <div class="card-body">
                    <ul class="list-group pl-5 list-group-flush" id="reply-${post.id}">
                        ${reply}
                    </ul>
                    <div class="form-group text-right mt-4">
                        <textarea class="form-control" placeholder="Masukkan komentar"
                            rows="3"></textarea>
                        <button data-id="${post.id}" type="button" class="btn btn-danger btn-sm mt-2 btn-reply">
                            <i class="bi bi-send"></i>
                            Send
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}

function getAllReplies(replies) {
    let reply = ''
    replies.map(_reply => {
        reply += /*html*/`
        <li class="list-group-item" style="border-color: #dc3545;">
            <div class="d-flex align-items-center">
                <img src="https://ui-avatars.com/api/?name=rudi" class="rounded-circle"
                    width="50px" alt="">
                <div class="ml-2" style="line-height: 15px;">
                    <p class="m-0 font-weight-bold">${_reply.nama}</p>
                    <small class="m-0">${_reply.username}</small>
                </div>
            </div>
            <div class="mt-3">
                <h6>${_reply.message}</</h6>
                <small class="">${_reply.created_at.split("T")[0]}</small>
            </div>
        </li>
        `
    })
    return reply
}
